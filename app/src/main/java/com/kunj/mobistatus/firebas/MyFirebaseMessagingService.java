package com.kunj.mobistatus.firebas;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;

import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private Context myContext;
    private NotificationCompat.Builder mBuilder;
    private String NOTIFICATION_CHANNEL_ID = "renus001";
    private NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        myContext = this;

//        if (remoteMessage.getData() != null) {
//
//
//
//        } else {
//            Log.d("+++++mess", "it have nthng");
//        }

       /* String remoteMasage = "{\n" +
                "  \"to\": \"device token\",\n" +
                "  \"notification\": {\n" +
                "    \"body\": \"Body of Static \",\n" +
                "    \"title\": \"Title Of Static\",\n" +
                "    \"icon\": \"myicon\",\n" +
                "    \"sound\": \"mySound\",\n" +
                "    \"contentId\": 12,\n" +
                "    \"categoryId\": 1\n" +
                "  }\n" +
                "}";*/

        Log.d("+++++mess", remoteMessage.getData().toString());

        JSONObject detailMessage = new JSONObject(remoteMessage.getData());
//        String DeviceToken = jsonObject.optString("to");
//        JSONObject detailMessage = jsonObject.optJSONObject("notification");

        String title = detailMessage.optString("title");
        String body = detailMessage.optString("body");
        int contentId = detailMessage.optInt("contentId");
        int categoryId = detailMessage.optInt("categoryId");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra("IsFromFirebase", true);
            resultIntent.putExtra("contentId", contentId);
            resultIntent.putExtra("CategoryId", categoryId);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            PendingIntent requestPendingIntent = PendingIntent.getActivity(myContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            mBuilder.setSmallIcon(R.drawable.ic_launcher_foreground);

            NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
            textStyle.bigText(body);

            mBuilder.setContentTitle(title)
                    .setContentTitle(title)
                    .setBadgeIconType(R.mipmap.ic_launcher_round)
                    .setContentText(body)
                    .setStyle(textStyle)
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .setAutoCancel(false)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(requestPendingIntent);
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NotificationChannel", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(100);
            notificationManager = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(0, mBuilder.build());
        } else {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "0");
            builder.setContentTitle(title);
            builder.setContentText(body);
            builder.setAutoCancel(true);
            builder.setSmallIcon(R.mipmap.ic_launcher_round);
            builder.setSound(uri);
            builder.setOnlyAlertOnce(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher_round));
                builder.setColor(this.getResources().getColor(R.color.white));
            }
            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra("IsFromFirebase", true);
            resultIntent.putExtra("contentId", contentId);
            resultIntent.putExtra("CategoryId", categoryId);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify((int) System.currentTimeMillis(), builder.build());
        }


    }

   /* private void sendnotification(String title, String body) {
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "0");
        builder.setContentTitle(title);
        builder.setContentText(body);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setSound(uri);
        builder.setOnlyAlertOnce(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher_round));
            builder.setColor(this.getResources().getColor(R.color.white));
        }
        Intent resultIntent = new Intent();
        resultIntent.putExtra("IsFromFirebase", true);
        resultIntent.putExtra("contentId", contentId);
        resultIntent.putExtra("CategoryId", categoryId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), builder.build());
    }*/

}