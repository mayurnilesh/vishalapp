package com.kunj.mobistatus.server;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orm.util.NamingHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.listners.ServerCallBacks;
import com.kunj.mobistatus.sugermodel.Applog;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.sugermodel.Language;
import com.kunj.mobistatus.sugermodel.Users;
import com.kunj.mobistatus.utils.AppConstans;

public class AppServer {
    private final static String TAG = "AppServer";

    public static void notifyServerNotification(RequestQueue queue, final boolean b, final ServerCallBacks callBack) {

        final List<Users> user = Users.listAll(Users.class);
        if (user == null || (user.size() > 0 && user.get(0) == null)) {
            callBack.onResponse(ServerCallBacks.STATUS_CANCEL, "You are not Registered", null);
            return;
        }

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {
                                Users userTemp = user.get(0);
                                userTemp.setIs_allow_notification(b ? 1 : 0);
                                userTemp.save();
                                callBack.onResponse(ServerCallBacks.STATUS_OK, jsonObject.getString("message"), response);
                            } else {
                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);

                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.NOTIFICATION);
                //params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                // params.put("device_id", user.get(0).getDevice_id());
                // params.put("device_token", user.get(0).getDevice_token());
                // params.put("fcm_token", user.get(0).getFcm_token());
                params.put("user_id", user.get(0).getId().toString());
                params.put("is_notification", b ? "1" : "0");
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);
    }

    public static void registerUser(RequestQueue queue, final String deviceToken, final ServerCallBacks callBack) {

        final String fcm_token = ApplicationSession.getFromPreference(AppConstans.FCM_TOKEN).isEmpty() ? "" : ApplicationSession.getFromPreference(AppConstans.FCM_TOKEN);
        Log.d("TokenFCM", "Going " + fcm_token);

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {
                                Users users = new Users();
                                users.setId(Long.parseLong(String.valueOf(jsonObject.optJSONObject("data").optInt("Rid"))));
                                users.setDevice_token(jsonObject.optJSONObject("data").optString("device_token"));
                                users.setDevice_type(jsonObject.optJSONObject("data").optString("device_type"));
                                users.setDevice_id(jsonObject.optJSONObject("data").optString("device_id"));
                                users.setFcm_token(fcm_token);
                                users.setIs_allow_notification(1);
                                users.save();
                                if (Users.listAll(Users.class).size() > 0) {
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_USER_REGISTER, true);
                                }
                                callBack.onResponse(ServerCallBacks.STATUS_OK, jsonObject.getString("message"), null);
                            } else {
                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.REGISTER);
                params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                params.put("device_id", deviceToken);
                params.put("fcm_token", fcm_token);
                return params;
            }
        };
        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }


    public static void synckOpenData(RequestQueue queue, final ServerCallBacks callBack) {
        final List<Users> user = Users.listAll(Users.class);
        if (user == null || (user.size() > 0 && user.get(0) == null)) {
            callBack.onResponse(ServerCallBacks.STATUS_CANCEL, "You are not Registered", null);
            return;
        }

        List<Applog> books = Applog.listAll(Applog.class);
        if (books.size() <= 0)
            return;
        Log.d(TAG, "onCreate: " + books.get(books.size() - 1).getCount());
        Log.d(TAG, "onCreate1: " + books.size());

        final JSONArray jsonArray = new JSONArray();
        try {
            for (Applog data : books) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("date", data.getDate());
                jsonObject.put("count", String.valueOf(data.getCount()));
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, jsonArray.toString());
        /*[{"date":"2010-10-10", "count":"10"}, {"date":"2018-10-10", "count":"10"}]*/

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        /* {"latest_version":"1.0","is_device_register":1,"log_update":1,"is_new_update":false,"is_language_update":false,"is_category_update":false,"is_content_update":false}
                         * */
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            // if (jsonObject.getInt("log_update") == 1) {
                            Applog.deleteAll(Applog.class);
                            callBack.onResponse(ServerCallBacks.STATUS_OK, "ok", response);

                            /*if (jsonObject.has("bgImage")) {
                                String url = jsonObject.optString("bgImage");
                                if (url != null && !url.isEmpty()) {
                                    ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL, url);
                                }
                            }*/
//                            } else {
//                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
//                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.UCHECK);
                params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                params.put("fcm_token", user.get(0).getDevice_token());
                params.put("user_id", String.valueOf(user.get(0).getId()));
                params.put("log_data", jsonArray.toString());
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }


    public static void getCategory(RequestQueue queue, final ServerCallBacks callBack) {

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {

                                List<Category> contentsApp = Category.findWithQuery(Category.class, "select * from " + NamingHelper.toSQLName(Category.class) + " order by id DESC limit 1 ");
                                Long maxVal = 0L;
                                if (contentsApp.size() > 0) {
                                    maxVal = contentsApp.get(0).getId();
                                }
                                JSONArray data = jsonObject.getJSONArray("category_list");
                                for (int n = 0; n < data.length(); n++) {
                                    JSONObject rowData = data.getJSONObject(n);
                                    int currentId = rowData.getInt("CategoryId");
                                    if (maxVal < currentId) {
                                        Category category = new Category();
                                        category.setId(Long.parseLong(String.valueOf(currentId)));
                                        category.setCategory_name(rowData.optString("CategoryName"));
                                        if (rowData.optString("BackgroundColor").isEmpty()) {
                                            category.setBg_color("#35dcf7");
                                        } else {
                                            category.setBg_color(rowData.optString("BackgroundColor"));
                                        }
                                        category.setLanguage_id(rowData.optInt("LanguageId"));
                                        category.setSort_order_no(rowData.optInt("SortOrder"));
                                        category.setStatus(rowData.optInt("Status"));
                                        category.save();
                                    } else {
                                        Category category = Category.findById(Category.class, currentId);
                                        if (category == null) {
                                            category = new Category();
                                            category.setId(Long.parseLong(String.valueOf(currentId)));
                                            category.setBg_color("#35dcf7");
                                        }
                                        category.setCategory_name(rowData.optString("CategoryName"));
                                        if (!rowData.optString("BackgroundColor").isEmpty()) {
                                            category.setBg_color(rowData.optString("BackgroundColor"));
                                        }
                                        category.setLanguage_id(rowData.optInt("LanguageId"));
                                        category.setSort_order_no(rowData.optInt("SortOrder"));
                                        category.setStatus(rowData.optInt("Status"));
                                        category.save();
                                    }
                                }

                                callBack.onResponse(ServerCallBacks.STATUS_OK, jsonObject.getString("message"), null);
                            } else {
                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.CATSYNC);
                // params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }

    public static void getLanguages(RequestQueue queue, final ServerCallBacks callBack) {

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {

                                List<Language> contentsApp = Language.findWithQuery(Language.class, "select * from " + NamingHelper.toSQLName(Language.class) + " order by id DESC limit 1 ");
                                Long maxVal = 0L;
                                if (contentsApp.size() > 0) {
                                    maxVal = contentsApp.get(0).getId();
                                }

                                JSONArray data = jsonObject.getJSONArray("langauge_list");
                                for (int n = 0; n < data.length(); n++) {
                                    JSONObject rowData = data.getJSONObject(n);
                                    int currentId = rowData.getInt("LanguageId");
                                    if (maxVal < currentId) {
                                        Language lan = new Language();
                                        lan.setId(Long.parseLong(String.valueOf(currentId)));
                                        lan.setLanguage_name(rowData.optString("LanguageName"));
                                        lan.setStatus(Integer.parseInt(rowData.optString("Status")));
                                        lan.save();
                                    } else {
                                        Language lan = Language.findById(Language.class, currentId);
                                        if (lan == null) {
                                            lan = new Language();
                                        }
                                        lan.setId(Long.parseLong(String.valueOf(currentId)));
                                        lan.setLanguage_name(rowData.optString("LanguageName"));
                                        lan.setStatus(Integer.parseInt(rowData.optString("Status")));
                                        lan.save();
                                    }
                                }

                                callBack.onResponse(ServerCallBacks.STATUS_OK, jsonObject.getString("message"), null);
                            } else {
                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.LANGSYNC);
                // params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }

    public static void getProducts(RequestQueue queue, final ServerCallBacks callBack) {

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {
                                //ContentsApp contentsApp = ContentsApp.findWithQuery(ContentsApp.class, "select MAX(id) from " + NamingHelper.toSQLName(ContentsApp.class));
                                List<ContentsApp> contentsApp = ContentsApp.findWithQuery(ContentsApp.class, "select * from " + NamingHelper.toSQLName(ContentsApp.class) + " order by id DESC limit 1 ");
                                Long maxVal = 0L;
                                if (contentsApp.size() > 0) {
                                    maxVal = contentsApp.get(0).getId();
                                }
                                JSONArray data = jsonObject.getJSONArray("content_list");
                                for (int n = 0; n < data.length(); n++) {
                                    JSONObject rowData = data.getJSONObject(n);
                                    int currentId = rowData.getInt("ContentId");
                                    if (currentId > maxVal) {
                                        ContentsApp content = new ContentsApp();
                                        content.setId(Long.parseLong(String.valueOf(currentId)));
                                        content.setCategory_id(Long.parseLong(String.valueOf(rowData.getInt("CategoryId"))));
                                        content.setStatus(Integer.parseInt(rowData.optString("Status")));
                                        content.setText(rowData.optString("TextContent"));
                                        content.save();
                                    } else {
                                        ContentsApp contentsApp1 = ContentsApp.findById(ContentsApp.class, currentId);
                                        if (contentsApp1 == null) {
                                            contentsApp1 = new ContentsApp();
                                            contentsApp1.setId(Long.parseLong(String.valueOf(currentId)));
                                        }
                                        contentsApp1.setId(Long.parseLong(String.valueOf(currentId)));
                                        contentsApp1.setCategory_id(Long.parseLong(String.valueOf(rowData.getInt("CategoryId"))));
                                        contentsApp1.setStatus(Integer.parseInt(rowData.optString("Status")));
                                        contentsApp1.setText(rowData.optString("TextContent"));
                                        contentsApp1.save();

                                    }
                                }

                                callBack.onResponse(ServerCallBacks.STATUS_OK, jsonObject.getString("message"), null);
                            } else {
                                callBack.onResponse(ServerCallBacks.STATUS_CANCEL, jsonObject.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onResponse(ServerCallBacks.STATUS_CANCEL, error.toString(), null);
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.CANTSYNC);
                //params.put("device_type", AppConstans.Registrarion.DEVICE_TYPE);
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }

}
