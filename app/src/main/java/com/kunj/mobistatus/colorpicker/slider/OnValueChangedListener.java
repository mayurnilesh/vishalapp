package com.kunj.mobistatus.colorpicker.slider;

public interface OnValueChangedListener {
	void onValueChanged(float value);
}