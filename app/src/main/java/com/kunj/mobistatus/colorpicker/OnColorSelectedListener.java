package com.kunj.mobistatus.colorpicker;

public interface OnColorSelectedListener {
	void onColorSelected(int selectedColor);
}
