package com.kunj.mobistatus.colorpicker.builder;


import com.kunj.mobistatus.colorpicker.ColorPickerView;
import com.kunj.mobistatus.colorpicker.renderer.ColorWheelRenderer;
import com.kunj.mobistatus.colorpicker.renderer.FlowerColorWheelRenderer;
import com.kunj.mobistatus.colorpicker.renderer.SimpleColorWheelRenderer;

public class ColorWheelRendererBuilder {
    public static ColorWheelRenderer getRenderer(ColorPickerView.WHEEL_TYPE wheelType) {
        switch (wheelType) {
            case CIRCLE:
                return new SimpleColorWheelRenderer();
            case FLOWER:
                return new FlowerColorWheelRenderer();
        }
        throw new IllegalArgumentException("wrong WHEEL_TYPE");
    }
}