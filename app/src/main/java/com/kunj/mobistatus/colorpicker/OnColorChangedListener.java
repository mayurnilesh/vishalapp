package com.kunj.mobistatus.colorpicker;

public interface OnColorChangedListener {
	void onColorChanged(int selectedColor);
}
