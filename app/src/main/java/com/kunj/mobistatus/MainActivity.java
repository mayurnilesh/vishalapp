package com.kunj.mobistatus;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.util.List;

import com.kunj.mobistatus.adapter.DrawerRecycler;
import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.app.ConnectionDetector;
import com.kunj.mobistatus.dialog.NewVersionDialog;
import com.kunj.mobistatus.dialog.RatingDialog;
import com.kunj.mobistatus.fragments.CategoryFragment;
import com.kunj.mobistatus.fragments.DisclaimerFragment;
import com.kunj.mobistatus.fragments.FavouriteListFragment;
import com.kunj.mobistatus.fragments.MainFragment;
import com.kunj.mobistatus.fragments.ProductFragment;
import com.kunj.mobistatus.fragments.ProductSlidingFragment;
import com.kunj.mobistatus.fragments.RateUSFragment;
import com.kunj.mobistatus.listners.AppCallBacks;
import com.kunj.mobistatus.listners.ServerCallBacks;
import com.kunj.mobistatus.server.AppServer;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.Language;
import com.kunj.mobistatus.sugermodel.Users;
import com.kunj.mobistatus.utils.AppConstans;
import com.kunj.mobistatus.utils.DrawerConstants;
import com.kunj.mobistatus.utils.Utility;

public class MainActivity extends AppCompatActivity implements DrawerRecycler.ItemListener {


    String TAG = "MainActivity";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private AdView mAdView;
    private FrameLayout containerMain;
    private int currentFragmentId = 0;
    private TextView headerTitle;
    public RequestQueue queue;
    private Switch switchNotification;
    private NewVersionDialog dialog;
    private String lastfragmentName = "";
    public int categoryLastPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_SHOW_RATING)) {
            RatingDialog rt = new RatingDialog(this);
            rt.show();

        }

        getWindow().setBackgroundDrawable(null);
        //ApplicationSession.setBoleanToPreference(AppConstans.IS_NEW_UPDATE, true);
        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_NEW_UPDATE)) {
            openLangauageSelectionDialog();
            return;
        }
        queue = Volley.newRequestQueue(this);
        headerTitle = findViewById(R.id.headerTitle);
        headerTitle.setText(getString(R.string.app_name));
        initDrawer();
        initialize();
        synckTodayData();
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("IsFromFirebase")) {
            //Toast.makeText(this, "Notification Sucessfully", Toast.LENGTH_LONG).show();
            int categoryId = getIntent().getIntExtra("CategoryId", 0);
            int contentId = getIntent().getIntExtra("contentId", 0);

            Category category = Category.findById(Category.class, categoryId);
            if (category != null) {
                Bundle bd = new Bundle();
                bd.putLong(AppConstans.CURRENT_CLICK_PRODUCT_ID, contentId);
                bd.putString(AppConstans.HEADER_TITLE, category.category_name);
                bd.putLong(AppConstans.PASSING_DATA, categoryId);
                changeFragment(AppConstans.HORIZONTAL_SLIDING_TITLES, bd);
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void synckTodayData() {
        if (getPermission(Manifest.permission.INTERNET, "Internet Access", "This permission is required up to date data and latest newa.", 2002)) {
            if (ConnectionDetector.isConnectingToInternet(this)) {
                if (!ApplicationSession.getBooleanFromPreference(AppConstans.IS_USER_REGISTER)) {
                    if (ApplicationSession.getFromPreference(AppConstans.FCM_TOKEN).isEmpty()) {
                        if (ApplicationSession.getFromPreference(AppConstans.FCM_TOKEN).isEmpty()) {
                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                            if (!task.isSuccessful()) {
                                                Log.d("TokenFCM", "getInstanceId failed", task.getException());
                                                return;
                                            }
                                            ApplicationSession.setToPreference(AppConstans.FCM_TOKEN, task.getResult().getToken());
                                            registerUser();
                                        }
                                    });
                        }
                    } else {
                        registerUser();
                    }
                } else {
                    AppServer.synckOpenData(queue, new ServerCallBacks() {
                        @Override
                        public void onResponse(int status, String message, String data) {
                            if (status == ServerCallBacks.STATUS_OK) {
                                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                                try {
                                    JSONObject jsonObject = new JSONObject(data);
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_LUNGAUGE_UPDATE, jsonObject.optBoolean(AppConstans.IS_LUNGAUGE_UPDATE));
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_CONTENT_UPDATE, jsonObject.optBoolean(AppConstans.IS_CONTENT_UPDATE));
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_CATEGORY_UPDATE, jsonObject.optBoolean(AppConstans.IS_CATEGORY_UPDATE));


                                    if (!jsonObject.optString("latest_version").equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                        ApplicationSession.setBoleanToPreference(AppConstans.IS_NEW_UPDATE, true);
                                        openLangauageSelectionDialog();
                                    }
                                    getNewData();
                                } catch (Exception e) {

                                }
                            }
                        }
                    });

                }

            }
        }
    }

    private void registerUser() {

        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        AppServer.registerUser(queue, androidId, new ServerCallBacks() {
            @Override
            public void onResponse(int status, String message, String data) {
                if (status == ServerCallBacks.STATUS_OK) {
                    AppServer.synckOpenData(queue, new ServerCallBacks() {
                        @Override
                        public void onResponse(int status, String message, String data) {
                            if (status == ServerCallBacks.STATUS_OK) {
                                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                                try {
                                    JSONObject jsonObject = new JSONObject(data);
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_LUNGAUGE_UPDATE, jsonObject.optBoolean(AppConstans.IS_LUNGAUGE_UPDATE));
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_CONTENT_UPDATE, jsonObject.optBoolean(AppConstans.IS_CONTENT_UPDATE));
                                    ApplicationSession.setBoleanToPreference(AppConstans.IS_CATEGORY_UPDATE, jsonObject.optBoolean(AppConstans.IS_CATEGORY_UPDATE));

                                    if (!jsonObject.optString("latest_version").equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                        ApplicationSession.setBoleanToPreference(AppConstans.IS_NEW_UPDATE, true);
                                        openLangauageSelectionDialog();
                                    }
                                    getNewData();
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
            }
        });

    }


    private void getNewData() {
        Log.d(TAG, "getnewData");
        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_CATEGORY_UPDATE)) {
            AppServer.getCategory(queue, new ServerCallBacks() {
                @Override
                public void onResponse(int status, String message, String data) {
                    if (status == ServerCallBacks.STATUS_CANCEL) {
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    } else if (status == ServerCallBacks.STATUS_OK) {
                        ApplicationSession.setBoleanToPreference(AppConstans.IS_CATEGORY_UPDATE, false);

                    }
                }
            });
        }

        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_LUNGAUGE_UPDATE)) {
            AppServer.getLanguages(queue, new ServerCallBacks() {
                @Override
                public void onResponse(int status, String message, String data) {
                    if (status == ServerCallBacks.STATUS_CANCEL) {
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

                    } else if (status == ServerCallBacks.STATUS_OK) {
                        ApplicationSession.setBoleanToPreference(AppConstans.IS_LUNGAUGE_UPDATE, false);
                    }
                }
            });
        }

        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_CONTENT_UPDATE)) {
            AppServer.getProducts(queue, new ServerCallBacks() {
                @Override
                public void onResponse(int status, String message, String data) {
                    if (status == ServerCallBacks.STATUS_CANCEL) {
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    } else if (status == ServerCallBacks.STATUS_OK) {
                        ApplicationSession.setBoleanToPreference(AppConstans.IS_CONTENT_UPDATE, false);
                    }
                }
            });
        }

    }

    private void initDrawer() {
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.nav_view);
        switchNotification = navigationView.findViewById(R.id.switchNotification);
        RecyclerView recycleDrawerMenu = navigationView.findViewById(R.id.recycleDrawerMenu);
        recycleDrawerMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        DrawerRecycler drawerRecycler = new DrawerRecycler(DrawerConstants.getDrawerData(this), this);
        recycleDrawerMenu.setAdapter(drawerRecycler);

        List<Users> user = Users.listAll(Users.class);
        if (user != null && (user.size() > 0 && user.get(0) != null)) {
            if (user.get(0).getIs_allow_notification() == 1) {
                switchNotification.setChecked(true);
            }
        } else {
            if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_NOTIFICATION_ENABLE)) {
                switchNotification.setChecked(true);
            }
        }

        findViewById(R.id.imgMenuClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.hideSoftKeyPad(view);
                drawerLayout.openDrawer(navigationView);
            }
        });
    }

    private void openLangauageSelectionDialog() {
        dialog = new NewVersionDialog(this, new AppCallBacks() {
            @Override
            public void onResponse(int status, String message, Object data) {
                MainActivity.this.finish();

            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawers();
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        super.onBackPressed();
    }


    private void initialize() {
        containerMain = findViewById(R.id.containerMain);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle bd = new Bundle();
        bd.putString(AppConstans.HEADER_TITLE, "Hindi");
        bd.putString(AppConstans.PASSING_DATA, "1");

        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bd);

        MainFragment mainFragment = new MainFragment();
        ft.add(containerMain.getId(), categoryFragment, CategoryFragment.FRAGMENT_TAG);
        ft.commit();
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (!ConnectionDetector.isConnectingToInternet(MainActivity.this)) {
                    Toast.makeText(MainActivity.this, "You currently not connected to internet.", Toast.LENGTH_LONG).show();
                    compoundButton.setChecked(!b);
                    return;
                }

                AppServer.notifyServerNotification(queue, b, new ServerCallBacks() {
                    @Override
                    public void onResponse(int status, String message, String data) {
                        if (status == ServerCallBacks.STATUS_OK) {
                            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    public void setHeader(String headerName) {
        if (headerName != null && !headerName.isEmpty()) {
            headerTitle.setText(headerName);
        }
    }

    public void changeFragment(int fragmentId, Bundle data) {
        if (currentFragmentId == fragmentId) {
            return;
        }
        categoryLastPosition = -1;
        Log.d("categoryLastPosition", categoryLastPosition + " Assign cahgenFragmnet ");
        //mAdView.setVisibility(View.VISIBLE);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (fragmentId) {
            case AppConstans.MAIN_FRAGMENT:
                lastfragmentName = MainFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), MainFragment.getInstance(data), lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.CATEGORY_FRAGMENT:
                lastfragmentName = CategoryFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), CategoryFragment.getInstance(data), lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.HORIZONTAL_SLIDING_TITLES:
                //mAdView.setVisibility(View.GONE);
                ProductSlidingFragment pr = new ProductSlidingFragment();
                pr.setArguments(data);
                lastfragmentName = ProductSlidingFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), pr, lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.PRODUCT_FRAGMNET:
                lastfragmentName = ProductFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), ProductFragment.getInstance(this, data), lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.FAVOURITE_LIST:
                lastfragmentName = FavouriteListFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), FavouriteListFragment.getInstance(data), lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.DISCLAIMER_FRAGMENT:
                DisclaimerFragment ds = new DisclaimerFragment();
                ds.setArguments(data);
                lastfragmentName = DisclaimerFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), ds, lastfragmentName).addToBackStack(lastfragmentName);
                break;
            case AppConstans.RATE_US_FRAGMENT:
                RateUSFragment rs = new RateUSFragment();
                rs.setArguments(data);
                lastfragmentName = RateUSFragment.FRAGMENT_TAG;
                ft.replace(containerMain.getId(), rs, lastfragmentName).addToBackStack(lastfragmentName);
                break;

        }

        ft.commit();
    }

    public boolean getPermission(final String permissions, String title, String message, final int permissionRequestCode) {
        int perMissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, permissions);
        if (perMissionCheck != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissions)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissions}, permissionRequestCode);
                            }
                        });
                builder.create().show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissions}, permissionRequestCode);
            }

            return false;
        }
        return true;
    }

    public void getFirstCategory(ServerCallBacks callbacks) {
        if (getPermission(Manifest.permission.INTERNET, "Internet Access", "This permission is required upto date data and latest newa.", 2002)) {
            if (ConnectionDetector.isConnectingToInternet(this)) {
                AppServer.getCategory(queue, callbacks);
            } else {
                Toast.makeText(MainActivity.this, "No InternetConnection", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onItemClick(String item, String id) {

        if (!id.isEmpty()) {
            Language language = Language.findById(Language.class, Long.parseLong(id));
            if (language != null) {
                drawerLayout.closeDrawers();

                //getSupportFragmentManager().popBackStack(0, 1);
                Bundle bd = new Bundle();
                bd.putString(AppConstans.HEADER_TITLE, language.getLanguage_name());
                bd.putString(AppConstans.PASSING_DATA, id);
                if (getSupportFragmentManager().findFragmentByTag(CategoryFragment.FRAGMENT_TAG) == null) {
                    getSupportFragmentManager().popBackStack(0, 1);
                    changeFragment(AppConstans.CATEGORY_FRAGMENT, bd);
                } else {
                    //getSupportFragmentManager().popBackStack(CategoryFragment.FRAGMENT_TAG, 0);
                    getSupportFragmentManager().popBackStack(0, 1);
                    CategoryFragment cat = (CategoryFragment) getSupportFragmentManager().findFragmentByTag(CategoryFragment.FRAGMENT_TAG);
                    cat.changeData(id, language.getLanguage_name());
                    headerTitle.setText(item);
                    /*FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(containerMain.getId(), cat, CategoryFragment.FRAGMENT_TAG).addToBackStack(lastfragmentName);
                    lastfragmentName = CategoryFragment.FRAGMENT_TAG;
                    ft.commit();*/

                }
            }
            return;
        }

        switch (item) {
            case DrawerConstants.nav_favourite:
                Bundle bd = new Bundle();
                bd.putString(AppConstans.HEADER_TITLE, "Favourite");
                changeFragment(AppConstans.FAVOURITE_LIST, bd);
                break;
            case DrawerConstants.nav_home:
                getSupportFragmentManager().popBackStack(0, 1);
                setHeader(getString(R.string.app_name));
                break;
            case DrawerConstants.nav_disclaimer:
                Bundle bd1 = new Bundle();
                bd1.putString(AppConstans.HEADER_TITLE, "Disclaimer");
                changeFragment(AppConstans.DISCLAIMER_FRAGMENT, bd1);
                break;
//            case DrawerConstants.nav_rate_us:
//                Bundle rateus = new Bundle();
//                rateus.putString(AppConstans.HEADER_TITLE, "Feedback");
//                changeFragment(AppConstans.RATE_US_FRAGMENT, rateus);
//                break;
            case DrawerConstants.nav_share:
                Utility.shareWithApp(this, AppConstans.APPURL, false);
                break;
        }
        drawerLayout.closeDrawers();
    }
}
