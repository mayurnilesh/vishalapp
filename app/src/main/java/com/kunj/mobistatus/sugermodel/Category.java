package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Category extends SugarRecord {

    @Unique
    public int id;
    public String category_name;
    public int language_id;
    public String bg_color;
    public int sort_order_no;
    public int status;

    public String cat_type = "blush";

    public String getCat_type() {
        return cat_type;
    }

    public void setCat_type(String cat_type) {
        this.cat_type = cat_type;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public int getSort_order_no() {
        return sort_order_no;
    }

    public void setSort_order_no(int sort_order_no) {
        this.sort_order_no = sort_order_no;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
