package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;

import java.io.Serializable;

public class ContentsApp extends SugarRecord implements Serializable {

    public Long category_id;
    public String text;
    public int status;
    public int is_read;
    public int is_favourite;

    public ContentsApp() {

    }

    public ContentsApp(Long category_id, String text, int status, int is_read, int is_favourite) {
        this.category_id = category_id;
        this.text = text;
        this.status = status;
        this.is_read = is_read;
        this.is_favourite = is_favourite;
    }


    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIs_read() {
        return is_read;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }

    public int getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(int is_favourite) {
        this.is_favourite = is_favourite;
    }
}
