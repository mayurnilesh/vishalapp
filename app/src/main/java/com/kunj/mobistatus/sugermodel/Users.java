package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Users extends SugarRecord {

    @Unique
    public int id;
    public String device_type;
    public String device_token;
    public int is_allow_notification;
    public String status;
    public String created_date;
    public String ip;
    public String device_id;
    public String fcm_token;

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }



    public int getUId() {
        return id;
    }

    public void setUId(int id) {
        this.id = id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public int getIs_allow_notification() {
        return is_allow_notification;
    }

    public void setIs_allow_notification(int is_allow_notification) {
        this.is_allow_notification = is_allow_notification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
