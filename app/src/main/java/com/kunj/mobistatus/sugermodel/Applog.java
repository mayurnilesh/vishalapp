package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;

public class Applog extends SugarRecord {

    public String date;
    public int count;

    public Applog() {
    }

    public Applog(String date, int count) {
        this.date = date;
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
