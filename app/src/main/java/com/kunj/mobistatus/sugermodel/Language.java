package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Language extends SugarRecord {

    @Unique
    public int id;
    public int status;
    public String language_name;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }
}
