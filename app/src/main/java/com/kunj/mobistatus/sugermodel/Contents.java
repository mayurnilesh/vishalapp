package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Contents extends SugarRecord {

    @Unique
    public int id;
    public String category_id;
    public String text;
    public int status;


    public int getContentId() {
        return id;
    }

    public void setContentId(int id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
