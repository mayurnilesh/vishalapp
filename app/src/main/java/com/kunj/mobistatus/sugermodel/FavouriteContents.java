package com.kunj.mobistatus.sugermodel;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class FavouriteContents extends SugarRecord {

    @Unique
    public int id;
    public int contents_id;

    public int getFCId() {
        return id;
    }

    public void setFCId(int id) {
        this.id = id;
    }

    public int getContents_id() {
        return contents_id;
    }

    public void setContents_id(int contents_id) {
        this.contents_id = contents_id;
    }
}
