package com.kunj.mobistatus;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.sugermodel.Applog;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.sugermodel.Language;
import com.kunj.mobistatus.utils.AppConstans;

public class SplashScreen extends AppCompatActivity {

    RequestQueue queue;
    ProgressBar pb;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        pb = findViewById(R.id.pb);
        //ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL,"https://wallpapertag.com/wallpaper/full/2/c/5/132345-free-android-background-1242x2208-photo.jpg");
        //ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL,"https://i.pinimg.com/736x/ae/3c/ce/ae3ccef5d71383108c68f60ab3440234--flower-backgrounds-black-backgrounds.jpg ");
        //ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL,"http://pavbca.com/walldb/original/f/4/1/32384.jpg");
        //ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL,"https://i.pinimg.com/originals/c5/3b/31/c53b314e9bdc65d8eeb2e8c71d918184.jpg");
        //ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL,"https://androidwalls.net/wp-content/uploads/2015/03/White%20Flower%20Petals%20Corner%20Light%20Blue%20Background%20Android%20Wallpaper.jpg");
        ApplicationSession.setToPreference(AppConstans.BACKGROUND_URL, "http://nerdsmagazine.com/wp-content/uploads/2012/07/Beautiful_Nature-Android-Wallpaper.jpg");
        if (ApplicationSession.getFromPreference(AppConstans.FCM_TOKEN).isEmpty()) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.d("TokenFCM", "getInstanceId failed", task.getException());
                                return;
                            }
                            ApplicationSession.setToPreference(AppConstans.FCM_TOKEN, task.getResult().getToken());
                        }
                    });
        }

        queue = Volley.newRequestQueue(this);

        if (ApplicationSession.getBooleanFromPreference(AppConstans.IS_SECOND_TIME))
            hanlerhandle();


    }

    private void hanlerhandle() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                List<Applog> books = Applog.listAll(Applog.class);
                if (books.size() > 0) {
                    if (books.get(books.size() - 1).getDate().equals(date)) {
                        Applog applog = Applog.findById(Applog.class, books.get(books.size() - 1).getId());
                        applog.count = books.get(books.size() - 1).getCount() + 1;
                        applog.save();
                    } else {
                        Applog applog = new Applog(date, 1);
                        applog.setId(System.currentTimeMillis());
                        applog.save();
                        if (!ApplicationSession.getBooleanFromPreference(AppConstans.IS_RATING_DONE)) {
                            ApplicationSession.setBoleanToPreference(AppConstans.IS_SHOW_RATING, true);
                        }
                    }
                } else {
                    Applog applog = new Applog(date, 1);
                    applog.setId(System.currentTimeMillis());
                    applog.save();
                    if (!ApplicationSession.getBooleanFromPreference(AppConstans.IS_RATING_DONE)) {
                        ApplicationSession.setBoleanToPreference(AppConstans.IS_SHOW_RATING, true);
                    }
                }
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                finish();
            }
        }, 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!ApplicationSession.getBooleanFromPreference(AppConstans.IS_SECOND_TIME)) {
            try {
                storeCategory(readJSONFromAsset("category"));
                storeLanguage(readJSONFromAsset("language"));
                storeContent(readJSONFromAsset("content"));
                ApplicationSession.setBoleanToPreference(AppConstans.IS_SECOND_TIME, true);
                ApplicationSession.setBoleanToPreference(AppConstans.IS_NOTIFICATION_ENABLE, true);
                hanlerhandle();

            } catch (Exception e) {
                Log.e("ParsingData", e.getMessage());
            }
        }
    }

    public String readJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    private void storeContent(String strContent) throws JSONException {
        JSONObject jsonObject = new JSONObject(strContent);
        JSONArray data = jsonObject.getJSONArray("content_list");
        for (int n = 0; n < data.length(); n++) {
            JSONObject rowData = data.getJSONObject(n);
            ContentsApp content = new ContentsApp();
            content.setId(Long.parseLong(String.valueOf(rowData.getInt("ContentId"))));
            content.setCategory_id(Long.parseLong(String.valueOf(rowData.getInt("CategoryId"))));
            content.setStatus(Integer.parseInt(rowData.optString("Status")));
            content.setText(rowData.optString("TextContent"));
            content.save();
        }
    }

    private void storeLanguage(String language) throws JSONException {
        JSONObject jsonObject = new JSONObject(language);
        JSONArray data = jsonObject.getJSONArray("langauge_list");
        for (int n = 0; n < data.length(); n++) {
            JSONObject rowData = data.getJSONObject(n);
            Language lan = new Language();
            lan.setId(Long.parseLong(String.valueOf(rowData.getInt("LanguageId"))));
            lan.setLanguage_name(rowData.optString("LanguageName"));
            lan.setStatus(Integer.parseInt(rowData.optString("Status")));
            lan.save();
        }
    }

    private void storeCategory(String strCategory) throws JSONException {
        String[] dataIcons = new String[]{"courage", "blush", "crying", "date", "idea", "joyful", "lovelorn", "shaking_hands", "wedding_cake"};

        JSONObject jsonObject = new JSONObject(strCategory);
        JSONArray data = jsonObject.getJSONArray("category_list");
        for (int n = 0; n < data.length(); n++) {
            JSONObject rowData = data.getJSONObject(n);
            Category category = new Category();
            category.setId(Long.parseLong(String.valueOf(rowData.getInt("CategoryId"))));
            category.setCategory_name(rowData.optString("CategoryName"));
            category.setBg_color(rowData.optString("BackgroundColor"));
            category.setLanguage_id(rowData.optInt("LanguageId"));
            category.setSort_order_no(rowData.optInt("SortOrder"));
            category.setStatus(rowData.optInt("Status"));
            category.setCat_type(dataIcons[new Random().nextInt(9)]);
            category.save();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pb.setVisibility(View.GONE);
    }
}
