package com.kunj.mobistatus;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.kunj.mobistatus.colorpicker.ColorPickerView;
import com.kunj.mobistatus.colorpicker.builder.ColorPickerClickListener;
import com.kunj.mobistatus.colorpicker.builder.ColorPickerDialogBuilder;
import com.kunj.mobistatus.text.widget.MotionView;
import com.kunj.mobistatus.text.widget.TextEditorDialogFragment;
import com.kunj.mobistatus.text.widget.adapter.FontsAdapter;
import com.kunj.mobistatus.text.widget.entity.ImageEntity;
import com.kunj.mobistatus.text.widget.entity.MotionEntity;
import com.kunj.mobistatus.text.widget.entity.TextEntity;
import com.kunj.mobistatus.text.widget.utils.FontProvider;
import com.kunj.mobistatus.text.widget.viewmodel.Font;
import com.kunj.mobistatus.text.widget.viewmodel.Layer;
import com.kunj.mobistatus.text.widget.viewmodel.TextLayer;

public class AddingStickerActivity extends AppCompatActivity implements TextEditorDialogFragment.OnTextLayerCallback {
    /*implementation 'com.theartofdev.edmodo:android-image-cropper:2.7.+'*/
    protected MotionView motionView;
    protected View textEntityEditPanel;
    private String textOnImage = "Sample Text";
    private SeekBar seekBarTextSize;
    private SeekBar seekBarBlurImage;
    private Bitmap backGroundBitmap;
    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {
            if (entity instanceof TextEntity) {
                textEntityEditPanel.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {
            startTextEntityEditing();
        }
    };
    private FontProvider fontProvider;
    private static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1001;
    String TAG = "AddingStickerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_sticker);

        init();
    }

    private int lastSeekBarvalue = 10;

    private void init() {
        this.fontProvider = new FontProvider(getResources());
        seekBarTextSize = findViewById(R.id.seekBarTextSize);
        seekBarBlurImage = findViewById(R.id.seekBarBlurImage);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("data")) {
            textOnImage = getIntent().getStringExtra("data");
        } else {
            textOnImage = "Sample Text";
        }

        motionView = findViewById(R.id.main_motion_view);
        Bitmap icon = BitmapFactory.decodeResource(this.getResources(), R.drawable.bg);
        Bitmap bitmap = icon.copy(Bitmap.Config.ARGB_8888, true);
        setMotionViewHeight(bitmap);
        textEntityEditPanel = findViewById(R.id.main_motion_text_entity_edit_panel);
        motionView.setMotionViewCallback(motionViewCallback);
        //motionView.setBackgroundResource(R.drawable.cute);

        initTextEntitiesListeners();
        seekBarTextSize.setMax(30);
        seekBarTextSize.setProgress(10);


        seekBarBlurImage.setMax(25);
        seekBarBlurImage.setProgress(0);


        seekBarBlurImage.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i > 0 && i <= 25) {
                    BitmapDrawable bm = new BitmapDrawable(blurImage(backGroundBitmap, i));
                    motionView.setBackgroundDrawable(bm);
                } else {
                    BitmapDrawable bm = new BitmapDrawable(backGroundBitmap);
                    motionView.setBackgroundDrawable(bm);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBarTextSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (lastSeekBarvalue < i) {
                    increaseTextEntitySize();
                } else {
                    decreaseTextEntitySize();
                }
                lastSeekBarvalue = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // addTextSticker();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "init: " + motionView.getHeight());
                addTextSticker();
            }
        }, 300);
    }

    private void setMotionViewHeight(Bitmap bmp) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        //addPhotosBitmap(icon);
        //int width = motionView.getWidth();
        if (bmp.getWidth() > width) {
            int newHeight = (width * bmp.getHeight()) / bmp.getWidth();
            ViewGroup.LayoutParams lp = motionView.getLayoutParams();
            lp.height = newHeight;
            lp.width = width;
            motionView.setLayoutParams(lp);

        } else {
            ViewGroup.LayoutParams lp = motionView.getLayoutParams();
            lp.height = bmp.getHeight();
            lp.width = bmp.getWidth();
            motionView.setLayoutParams(lp);
        }
        backGroundBitmap = bmp;
        BitmapDrawable bmpDrw = new BitmapDrawable(bmp);
        motionView.setBackground(bmpDrw);
    }

    private void initTextEntitiesListeners() {

        findViewById(R.id.fetchImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(AddingStickerActivity.this);
            }
        });


        findViewById(R.id.text_entity_color_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityColor();
            }
        });
        findViewById(R.id.text_entity_font_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityFont();
            }
        });
        findViewById(R.id.text_entity_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "init: " + motionView.getHeight());
                //addTextSticker();
                startTextEntityEditing();
                findViewById(R.id.llTextSize).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(AddingStickerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    final String[] array = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddingStickerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddingStickerActivity.this);
                            builder.setTitle("Read Phone State")
                                    .setMessage("Need Permission")
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ActivityCompat.requestPermissions(AddingStickerActivity.this,
                                                    array, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                                        }
                                    });
                            builder.create().show();


                        }

                        ActivityCompat.requestPermissions(AddingStickerActivity.this, array, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                        //saveImage();
                    } else {
                        ActivityCompat.requestPermissions(AddingStickerActivity.this, array, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    }
                } else {
                    saveImage();
                }
            }
        });

    }


    public void saveImage() {

        try {
            motionView.unselectEntity();

            File imagePath = new File(Environment.getExternalStorageDirectory(), "MobiStatus");
            Log.d("TAG", "onClick: " + imagePath.toString() + " " + imagePath.exists());
            if (!imagePath.exists()) {
                Log.d("TAG", "onClick: " + imagePath.mkdir());
            }
            //File sketch = new File(imagePath, "/testing.png");
            Bitmap imageFile = getViewBitmap(motionView);

            String imgpath = imagePath + "/mobistatus" + System.nanoTime() + ".jpg";
            FileOutputStream fos = new FileOutputStream(imgpath);
            imageFile.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            MediaScannerConnection.scanFile(this, new String[]{imgpath}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });

            Toast.makeText(AddingStickerActivity.this, "Image save successfully", Toast.LENGTH_LONG).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    @Nullable
    private TextEntity currentTextEntity() {
        if (motionView != null && motionView.getSelectedEntity() instanceof TextEntity) {
            return ((TextEntity) motionView.getSelectedEntity());
        } else {
            return null;
        }
    }

    protected void addTextSticker() {
        TextLayer textLayer = createTextLayer();
        TextEntity textEntity = new TextEntity(textLayer, motionView.getWidth(),
                motionView.getHeight(), fontProvider);
        motionView.addEntityAndPosition(textEntity);

        // move text sticker up so that its not hidden under keyboard
        PointF center = textEntity.absoluteCenter();
        center.y = center.y * 0.5F;
        textEntity.moveCenterTo(center);

        // redraw
        motionView.invalidate();

        //startTextEntityEditing();
    }

    private TextLayer createTextLayer() {
        TextLayer textLayer = new TextLayer();
        Font font = new Font();

        font.setColor(TextLayer.Limits.INITIAL_FONT_COLOR);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);
        font.setTypeface(fontProvider.getDefaultFontName());
        textLayer.setFont(font);

        textLayer.setText(textOnImage);
        return textLayer;
    }


    private void increaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().increaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
        }
    }

    private void decreaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().decreaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
        }
    }

    private void changeTextEntityColor() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity == null) {
            return;
        }

        int initialColor = textEntity.getLayer().getFont().getColor();

        ColorPickerDialogBuilder
                .with(AddingStickerActivity.this)
                .setTitle(R.string.select_color)
                .initialColor(initialColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(8) // magic number
                .setPositiveButton(R.string.ok, new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setColor(selectedColor);
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    private void changeTextEntityFont() {
        final List<String> fonts = fontProvider.getFontNames();
        FontsAdapter fontsAdapter = new FontsAdapter(this, fonts, fontProvider);
        new AlertDialog.Builder(this)
                .setTitle(R.string.select_font)
                .setAdapter(fontsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setTypeface(fonts.get(which));
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .show();
    }

    private void startTextEntityEditing() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextEditorDialogFragment fragment = TextEditorDialogFragment.getInstance(textEntity.getLayer().getText());
            fragment.show(getFragmentManager(), TextEditorDialogFragment.class.getName());
        }
    }

    private void addPhotos(final int stickerResId) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();
                Bitmap pica = BitmapFactory.decodeResource(getResources(), stickerResId);

                ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());

                motionView.addEntity(entity);
            }
        });
    }

    private void addPhotosBitmap(final Bitmap bmp) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();


                ImageEntity entity = new ImageEntity(layer, bmp, motionView.getWidth(), motionView.getHeight());

                motionView.addEntity(entity);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            try {
                Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), CropImage.getActivityResult(data).getUri());
                //BitmapDrawable ob = new BitmapDrawable(getResources(), imageBitmap);
                backGroundBitmap = imageBitmap;
                setMotionViewHeight(imageBitmap);
                //motionView.setBackgroundDrawable(ob);

                //findViewById(R.id.fetchImage).setVisibility(View.GONE);
                //textEntityEditPanel.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveImage();
            } else {
                Toast.makeText(AddingStickerActivity.this, "Opps! Permission Denied", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void textChanged(@NonNull String text) {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextLayer textLayer = textEntity.getLayer();
            if (!text.equals(textLayer.getText())) {
                textLayer.setText(text);
                textEntity.updateEntity();
                motionView.invalidate();
            }
        }
    }

 /*   private void blur(Bitmap bkg, int height, int width, int valueOfBluring) {
        long startMs = System.currentTimeMillis();
        float radius = 20;

        Bitmap overlay = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(overlay);
        canvas.translate(-view.getLeft(), -view.getTop());
        canvas.drawBitmap(bkg, 0, 0, null);
        overlay = FastBlur.doBlur(overlay, (int) radius, true);
        view.setBackground(new BitmapDrawable(getResources(), overlay));
        statusText.setText(System.currentTimeMillis() - startMs + "ms");
    }

*/

    public Bitmap blurImage(Bitmap image, float BLUR_RADIUS) {

        final float BITMAP_SCALE = 0.6f;

        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(AddingStickerActivity.this);

        ScriptIntrinsicBlur intrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);

        intrinsicBlur.setRadius(BLUR_RADIUS);
        intrinsicBlur.setInput(tmpIn);
        intrinsicBlur.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }
}
