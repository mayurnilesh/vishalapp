package com.kunj.mobistatus.dialog;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.kunj.mobistatus.R;
import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.listners.AppCallBacks;
import com.kunj.mobistatus.utils.AppConstans;

public class NewVersionDialog extends Dialog {
    AppCallBacks callBacks;
    TextView textNewVersion;
    TextView txtDone;
    Context context;

    public NewVersionDialog(Context context, AppCallBacks callBacks) {
        super(context);
        this.callBacks = callBacks;
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.simple_text_dialog);
        setCancelable(false);
        textNewVersion = findViewById(R.id.textNewVersion);
        txtDone = findViewById(R.id.txtDone);
        textNewVersion.setText(getContext().getResources().getString(R.string.new_version));
        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewVersionDialog.this.dismiss();
                Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    context.startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                }
                callBacks.onResponse(AppCallBacks.STATUS_OK, "", null);
            }
        });


    }
}
