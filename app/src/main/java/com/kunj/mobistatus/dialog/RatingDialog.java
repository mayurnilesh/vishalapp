package com.kunj.mobistatus.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.app.ConnectionDetector;
import com.kunj.mobistatus.sugermodel.Users;
import com.kunj.mobistatus.utils.AppConstans;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RatingDialog extends Dialog {


    private EditText edtComment;
    private ProgressBar progressRate;
    private Context context;
    private boolean isCallrunning = false;
    private boolean rateUs = false;

    public RatingDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_rate_us);
        init();
    }

    private void init() {
        edtComment = findViewById(R.id.edtComment);
        progressRate = findViewById(R.id.progressRate);
        setClicks();
    }

    private void setClicks() {
        findViewById(R.id.txtLater).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingDialog.this.dismiss();

            }
        });

        findViewById(R.id.txtSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateUs();

            }
        });

        findViewById(R.id.txtPlayStore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingDialog.this.dismiss();
                ApplicationSession.setBoleanToPreference(AppConstans.IS_SHOW_RATING, false);
            }
        });
    }

    private void rateUs() {

        if (!ConnectionDetector.isConnectingToInternet(getContext())) {
            Toast.makeText(context, "Please, Check your Internet connection and try again", Toast.LENGTH_LONG).show();
            return;
        }

        ApplicationSession.setBoleanToPreference(AppConstans.IS_RATING_DONE, true);
        ApplicationSession.setBoleanToPreference(AppConstans.IS_SHOW_RATING, false);
        RatingDialog.this.dismiss();
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }


    }

    public void rateUSRequestCall(RequestQueue queue, final Float rate, final String message) {
        final List<Users> user = Users.listAll(Users.class);
        if (user == null || (user.size() > 0 && user.get(0) == null)) {
            Toast.makeText(context, "You are not Registered", Toast.LENGTH_LONG).show();
            return;
        }

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {
                                progressRate.setVisibility(View.GONE);
                                isCallrunning = false;
                                ApplicationSession.setBoleanToPreference(AppConstans.IS_RATING_DONE, true);
                                ApplicationSession.setBoleanToPreference(AppConstans.IS_SHOW_RATING, false);
                                RatingDialog.this.dismiss();
                                Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                                Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    context.startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressRate.setVisibility(View.GONE);
                            isCallrunning = false;
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressRate.setVisibility(View.GONE);
                        isCallrunning = false;
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.URATE);
                params.put("comment", message);
                params.put("rating", String.valueOf(rate));
                params.put("user_id", String.valueOf(user.get(0).getId()));
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }
}
