package com.kunj.mobistatus.listners;

public interface ServerCallBacks {

    public static final int STATUS_OK = 1;
    public static final int STATUS_CANCEL = 2;

    public void onResponse(int status, String message, String data);
}
