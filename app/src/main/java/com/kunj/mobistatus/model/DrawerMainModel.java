package com.kunj.mobistatus.model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public class DrawerMainModel {

    public String name;
    public Drawable resurceId;
    public boolean isDropDown;
    public ArrayList<DrawerSubItem> subitems = new ArrayList<>();
}
