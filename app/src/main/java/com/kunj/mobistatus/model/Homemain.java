package com.kunj.mobistatus.model;

import java.util.ArrayList;

import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.Language;

public class Homemain {


    Language language;
    ArrayList<Category> categories;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
