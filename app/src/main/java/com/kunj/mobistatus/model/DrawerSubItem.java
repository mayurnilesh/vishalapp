package com.kunj.mobistatus.model;

public class DrawerSubItem {


    public String name;
    public String parentName;
    public String id;

    public DrawerSubItem(String name, String parentName, String id) {
        this.name = name;
        this.parentName = parentName;
        this.id = id;
    }

    public DrawerSubItem(String name, String parentName) {
        this.name = name;
        this.parentName = parentName;

    }

}
