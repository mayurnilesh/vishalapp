package com.kunj.mobistatus.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orm.util.NamingHelper;

import java.util.ArrayList;
import java.util.List;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.adapter.HomeAdpter;
import com.kunj.mobistatus.listners.ServerCallBacks;
import com.kunj.mobistatus.model.Homemain;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.Language;
import com.kunj.mobistatus.utils.AppConstans;

public class MainFragment extends Fragment {
    public final static String FRAGMENT_TAG = "MainFragment";
    static MainFragment INSTANCE;
    RecyclerView rec_list;

    View v;
    ArrayList<String> colorList = new ArrayList<>();

    private String headerName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = (String) bd.get(AppConstans.HEADER_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setHeader(getString(R.string.app_name));
        if (v == null) {
            v = inflater.inflate(R.layout.fragmnet_main, container, false);
            initialize(v);
            initializeData();
        }
        return v;

    }

    private void initialize(View v) {
        rec_list = v.findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(v.getContext());
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());

    }


    public void changeListData(ArrayList<Homemain> homemainArrayList) {
        HomeAdpter apercuAdapter = new HomeAdpter(homemainArrayList);
        rec_list.setAdapter(apercuAdapter);
    }

    public void newData() {
        initializeData();
    }


    private void initializeData() {

        ArrayList<Homemain> homemainArrayList = new ArrayList<>();
        String query = "select * from " + NamingHelper.toSQLName(Language.class) + " where " + NamingHelper.toSQLNameDefault("status ") + " = 1";
        List<Language> languages = Language.findWithQuery(Language.class, query);

        if (languages.size() > 0) {
            for (int i = 0; i < languages.size(); i++) {
                Log.d("MainFragmentLanguage", String.valueOf(languages.get(i).status));

                Homemain homemain = new Homemain();
                String queary = "select * from " + NamingHelper.toSQLName(Category.class) + " where " + NamingHelper.toSQLNameDefault("language_id") + " = ? and "+ NamingHelper.toSQLNameDefault("status ") + " = 1 limit 7";
                List<Category> categories = Category.findWithQuery(Category.class, queary, String.valueOf(languages.get(i).getId()));
                if (categories.size() > 0) {
                    homemain.setLanguage(languages.get(i));
                    homemain.setCategories((ArrayList<Category>) categories);
                    homemainArrayList.add(homemain);
                }
            }
            changeListData(homemainArrayList);
        }
        if (homemainArrayList.size() < 0 && languages.size() > 0) {
            ((MainActivity) getActivity()).getFirstCategory(new ServerCallBacks() {
                @Override
                public void onResponse(int status, String message, String data) {
                    if (status == ServerCallBacks.STATUS_OK) {
                        initializeData();
                    }
                }
            });
        }
    }

    public static MainFragment getInstance(Bundle data) {
        if (INSTANCE == null) {
            INSTANCE = new MainFragment();
            INSTANCE.setArguments(data);
        }
        return INSTANCE;
    }
}
