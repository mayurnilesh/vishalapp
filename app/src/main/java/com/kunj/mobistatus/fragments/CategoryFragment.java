package com.kunj.mobistatus.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.InterstitialAd;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.adapter.CategoryAdpter;
import com.kunj.mobistatus.app.ApplicationSession;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.utils.AppConstans;
import com.orm.util.NamingHelper;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {

    public final static String FRAGMENT_TAG = "CategoryFragment";
    static CategoryFragment INSTANCE;
    RecyclerView rec_list;
    ArrayList<Category> homesubArrayList = new ArrayList<>();
    private String headerName;
    private Long categoryID = -1L;
    private TextView txtNoRecordFound;
    private InterstitialAd mInterstitialAd;
    private RelativeLayout mainContainer;

    private BitmapDrawable isImageLoaded;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = (String) bd.get(AppConstans.HEADER_TITLE);
            if (bd.containsKey(AppConstans.PASSING_DATA)) {
                categoryID = Long.parseLong(String.valueOf(bd.get(AppConstans.PASSING_DATA)));
            }
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_category, container, false);
        ((MainActivity) getActivity()).setHeader(headerName);
        initialize(v);
        initializeData();

        return v;
    }

    private void initialize(View v) {

        rec_list = v.findViewById(R.id.rec);
        txtNoRecordFound = v.findViewById(R.id.txtNoRecordFound);
        mainContainer = v.findViewById(R.id.mainContainer);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(v.getContext(), 2);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
    }

    public void changeData(String id, String headerName) {
        this.headerName = headerName;
        categoryID = Long.valueOf(id);

        initializeData();

    }

    private void setData() {
        if (homesubArrayList != null && homesubArrayList.size() > 0) {
            txtNoRecordFound.setVisibility(View.GONE);
            CategoryAdpter apercuAdapter = new CategoryAdpter(homesubArrayList);
            rec_list.setAdapter(apercuAdapter);
        } else {
            txtNoRecordFound.setVisibility(View.VISIBLE);
        }

    }

    private void setBackgroundImage(final String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                BitmapDrawable bmp = new BitmapDrawable(bitmap);
                mainContainer.setBackground(bmp);
                isImageLoaded = bmp;
            }
        }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(imageRequest);
    }

    private void initializeData() {
        if (categoryID != null && categoryID > 0L) {
            String queary = "select * from " + NamingHelper.toSQLName(Category.class) + " where " + NamingHelper.toSQLNameDefault("language_id") + " = ? and " + NamingHelper.toSQLNameDefault("status ") + " = 1";
            homesubArrayList = (ArrayList<Category>) Category.findWithQuery(Category.class, queary, String.valueOf(categoryID));
            setData();
        }
        if (isImageLoaded == null) {
            if (!ApplicationSession.getFromPreference(AppConstans.BACKGROUND_URL).isEmpty()) {
                setBackgroundImage(ApplicationSession.getFromPreference(AppConstans.BACKGROUND_URL));
            }
        }else{
            mainContainer.setBackground(isImageLoaded);
        }

    }

    public static CategoryFragment getInstance(Bundle data) {

        if (INSTANCE == null) {
            INSTANCE = new CategoryFragment();
        }
        INSTANCE.setArguments(data);
        return INSTANCE;
    }
}
