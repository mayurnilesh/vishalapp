package com.kunj.mobistatus.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.adapter.ProductAdpter;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.utils.AppConstans;
import com.orm.util.NamingHelper;

import java.util.List;

public class ProductFragment extends Fragment {

    public final static String FRAGMENT_TAG = "ProductFragment";

    //static ProductFragment INSTANCE;
    RecyclerView relProduct;
    String headerTitle;
    ProductAdpter apercuAdapter;
    private String headerName;
    private Long categoryId = 0L;
    private TextView txtNoRecordFound;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = (String) bd.get(AppConstans.HEADER_TITLE);
            categoryId = bd.getLong(AppConstans.PASSING_DATA);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product, container, false);
        ((MainActivity) getActivity()).setHeader(headerName);
        initialize(v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (relProduct != null && apercuAdapter != null) {
            txtNoRecordFound.setVisibility(View.GONE);
            String quary = "Select * from " + NamingHelper.toSQLName(ContentsApp.class) + " where " + NamingHelper.toSQLNameDefault("category_id") + " = ?";
            List<ContentsApp> contents = ContentsApp.findWithQuery(ContentsApp.class, quary, String.valueOf(categoryId));
            apercuAdapter.setData(contents);
        } else {
            txtNoRecordFound.setVisibility(View.VISIBLE);
        }*/
    }


    private void initialize(View v) {
        txtNoRecordFound = v.findViewById(R.id.txtNoRecordFound);
        String quary = "Select * from " + NamingHelper.toSQLName(ContentsApp.class) + " where " + NamingHelper.toSQLNameDefault("category_id") + " = ? and " + NamingHelper.toSQLNameDefault("status ") + " = 1 order by id DESC ";
        List<ContentsApp> contents = ContentsApp.findWithQuery(ContentsApp.class, quary, String.valueOf(categoryId));
        if (contents.size() > 0) {
            txtNoRecordFound.setVisibility(View.GONE);
            relProduct = v.findViewById(R.id.relProduct);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(v.getContext());
            relProduct.setLayoutManager(linearLayoutManager);
            relProduct.setItemAnimator(new DefaultItemAnimator());
            apercuAdapter = new ProductAdpter(contents, headerTitle);
            relProduct.setAdapter(apercuAdapter);

            int lastPositionCat = ((MainActivity) getActivity()).categoryLastPosition;

            Log.d("categoryLastPosition", lastPositionCat + " In Use  Product Fragment ");
            if (((MainActivity) getActivity()).categoryLastPosition > 0) {
                if (relProduct.getAdapter().getItemCount() > lastPositionCat) {
                    relProduct.smoothScrollToPosition(lastPositionCat);
                }
            } else {
                relProduct.smoothScrollToPosition(0);
            }
            ((MainActivity) getActivity()).categoryLastPosition = -1;
            Log.d("categoryLastPosition", ((MainActivity) getActivity()).categoryLastPosition + " ssign product fragment");

        } else {
            txtNoRecordFound.setVisibility(View.VISIBLE);
        }
    }


    public static ProductFragment getInstance(MainActivity activity, Bundle data) {
        ProductFragment fm = (ProductFragment) activity.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (fm == null) {
            fm = new ProductFragment();
        }
        fm.setArguments(data);


        /*if (INSTANCE == null) {
            INSTANCE = new ProductFragment();

        }
        INSTANCE.setArguments(data);*/
        return fm;
    }
}
