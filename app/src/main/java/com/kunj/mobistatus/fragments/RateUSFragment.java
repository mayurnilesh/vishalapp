package com.kunj.mobistatus.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.app.ConnectionDetector;
import com.kunj.mobistatus.sugermodel.Users;
import com.kunj.mobistatus.utils.AppConstans;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RateUSFragment extends Fragment {

    public final static String FRAGMENT_TAG = "RateUSFragment";

    private String headerName;
    private TextView edtComment;
    private TextView txtSubmit;
    private TextView edtTitle;
    private RatingBar ratingBar;
    private ProgressBar progressRate;
    private boolean isCallrunning = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = (String) bd.get(AppConstans.HEADER_TITLE);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_rate_us, container, false);
        ((MainActivity) getActivity()).setHeader(headerName);
        init(v);
        return v;
    }

    private void init(View v) {
        edtComment = v.findViewById(R.id.edtComment);
        ratingBar = v.findViewById(R.id.ratingBar);
        txtSubmit = v.findViewById(R.id.txtSubmit);
        edtTitle = v.findViewById(R.id.edtTitle);
        progressRate = v.findViewById(R.id.progressRate);
        ratingBar.setRating(5.0f);

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateUs();
            }
        });
    }

    private boolean rateUs = false;

    private void rateUs() {
        float rate = ratingBar.getRating();
        String message = edtComment.getText().toString();
        String title = edtTitle.getText().toString();

        if (message.isEmpty()) {
            edtComment.requestFocus();
            edtComment.setError("Required");
            rateUs = true;
            return;
        }
        if (title.isEmpty()) {
            edtTitle.requestFocus();
            edtTitle.setError("Required");
            rateUs = true;
            return;
        }

        if (!ConnectionDetector.isConnectingToInternet(getContext())) {
            Toast.makeText(getActivity(), "Please, Check your Internet connection and try again", Toast.LENGTH_LONG).show();
            return;
        }

        if (isCallrunning) return;

        progressRate.setVisibility(View.VISIBLE);
        isCallrunning = true;

        rateUSRequestCall(((MainActivity) getActivity()).queue, title, message);


    }


    public void rateUSRequestCall(RequestQueue queue, final String title, final String message) {
        final List<Users> user = Users.listAll(Users.class);
        if (user == null || (user.size() > 0 && user.get(0) == null)) {
            Toast.makeText(getActivity(), "You are not Registered", Toast.LENGTH_LONG).show();
            return;
        }

        StringRequest strRequest = new StringRequest(Request.Method.POST, AppConstans.API_REGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 1) {
                                progressRate.setVisibility(View.GONE);
                                isCallrunning = false;
                                //  ApplicationSession.setBoleanToPreference(AppConstans.IS_RATING_DONE, true);
                                Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                                getActivity().onBackPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressRate.setVisibility(View.GONE);
                            isCallrunning = false;
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressRate.setVisibility(View.GONE);
                        isCallrunning = false;
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @SuppressLint("HardwareIds")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", AppConstans.Registrarion.UFEEDBACK);
                params.put("comment", message);
                params.put("title", title);
                params.put("user_id", String.valueOf(user.get(0).getId()));
                return params;
            }
        };

        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(strRequest);

    }

}
