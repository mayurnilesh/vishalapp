package com.kunj.mobistatus.fragments;

import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.orm.util.NamingHelper;

import java.util.List;

import com.kunj.mobistatus.AddingStickerActivity;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.adapter.MyCustomPagerAdapterObject;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.utils.AppConstans;
import com.kunj.mobistatus.utils.Utility;

import org.w3c.dom.Text;

import static android.content.Context.CLIPBOARD_SERVICE;

public class FavouriteListFragment extends Fragment implements View.OnClickListener {

    public final static String FRAGMENT_TAG = "FavouriteListFragment";
    private static FavouriteListFragment INSTANCE;
    private String headerName;
    ViewPager viewPager;
    private TextView pnumber;
    private static Long currentPage = 0L;
    private static ContentsApp lastPage;
    private MyCustomPagerAdapterObject adapter;
    private ImageView imgFavoriteIcon;
    private TextView txtNoRecordFound;
    private LinearLayout rlbottom;
    private AdView mAdView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = bd.getString(AppConstans.HEADER_TITLE);
            currentPage = bd.getLong(AppConstans.CURRENT_CLICK_PRODUCT_ID, 0L);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favorit_list, container, false);
        ((MainActivity) getActivity()).setHeader(headerName);
        init(v);
        intiAdd(v);
        return v;
    }

    private void intiAdd(View v) {
        mAdView = v.findViewById(R.id.adView);
        MobileAds.initialize(v.getContext(), getString(R.string.app_id));
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void init(View v) {
        TextView imgSetSticker = v.findViewById(R.id.imgSetSticker);
        imgSetSticker.setOnClickListener(this);
        imgFavoriteIcon = v.findViewById(R.id.imgFavoriteIcon);
        imgFavoriteIcon.setOnClickListener(this);
        ImageView imgShareWhatsApp = v.findViewById(R.id.imgShareWhatsApp);
        imgShareWhatsApp.setOnClickListener(this);
        ImageView imgShatreAllApp = v.findViewById(R.id.imgShatreAllApp);
        imgShatreAllApp.setOnClickListener(this);
        (v.findViewById(R.id.imgClipData)).setOnClickListener(this);
        txtNoRecordFound = v.findViewById(R.id.txtNoRecordFound);
        rlbottom = v.findViewById(R.id.rlbottom);

        viewPager = v.findViewById(R.id.pager);
        pnumber = v.findViewById(R.id.pnumber);
        String queary = "select * from " + NamingHelper.toSQLName(ContentsApp.class) + " where " + NamingHelper.toSQLNameDefault("is_favourite") + " = ? order by id DESC";

        List<ContentsApp> contents = ContentsApp.findWithQuery(ContentsApp.class, queary, "1");

        if (contents.size() > 0) {
            rlbottom.setVisibility(View.VISIBLE);
            txtNoRecordFound.setVisibility(View.GONE);
            adapter = new MyCustomPagerAdapterObject(v.getContext(), contents);
            viewPager.setAdapter(adapter);
            int noOfPage = adapter.getItemPositionOnId(currentPage);
            viewPager.setCurrentItem(noOfPage);
            lastPage = adapter.getCurrentObjectData(noOfPage);
            lastPage.is_read = 1;
            lastPage.save();
            if (lastPage.is_favourite == 1) {
                imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
            }
            pnumber.setText((noOfPage + 1) + "/" + contents.size());
        } else {
            txtNoRecordFound.setVisibility(View.VISIBLE);
            rlbottom.setVisibility(View.GONE);
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                int total = viewPager.getAdapter().getCount();
                pnumber.setText(String.format("%d / %d", i + 1, total));
                lastPage = adapter.getCurrentObjectData(viewPager.getCurrentItem());
                if (lastPage.is_favourite == 1) {
                    imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
                } else {
                    imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red));
                }
                if (lastPage.is_read == 0) {
                    lastPage.is_read = 1;
                    lastPage.save();
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
    }

    public static FavouriteListFragment getInstance(Bundle data) {

        if (INSTANCE == null) {
            INSTANCE = new FavouriteListFragment();
        }
        INSTANCE.setArguments(data);
        return INSTANCE;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgSetSticker:
                if (viewPager != null && adapter != null && adapter.getCurrentObjectData(viewPager.getCurrentItem()) != null) {
                    String data = adapter.getCurrentObjectData(viewPager.getCurrentItem()).text;
                    startActivity(new Intent(view.getContext(), AddingStickerActivity.class).putExtra("data", data));
                }
                break;
            case R.id.imgFavoriteIcon:
                if (lastPage != null) {
                    if (lastPage.is_favourite == 1) {
                        imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red));
                        lastPage.setIs_favourite(0);
                        lastPage.save();
                    } else {
                        imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
                        lastPage.setIs_favourite(1);
                        lastPage.save();
                    }
                }
                break;
            case R.id.imgShareWhatsApp:
                String textUrl = AppConstans.APPURL + "\n\n" + lastPage.text;
                Utility.shareWithApp(view.getContext(), textUrl, true);
                break;
            case R.id.imgShatreAllApp:
                String textUrl1 = AppConstans.APPURL + "\n\n" + lastPage.text;
                Utility.shareWithApp(view.getContext(), textUrl1, false);
                break;
            case R.id.imgClipData:
                ClipboardManager clipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(AppConstans.APPURL + "\n\n" + lastPage.text);
                Toast.makeText(view.getContext(), "Status clip successfully", Toast.LENGTH_LONG).show();
                break;
        }
    }
}