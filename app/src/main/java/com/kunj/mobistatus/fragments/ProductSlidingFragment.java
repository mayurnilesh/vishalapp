package com.kunj.mobistatus.fragments;

import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.orm.util.NamingHelper;

import java.util.ArrayList;
import java.util.List;

import com.kunj.mobistatus.AddingStickerActivity;
import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.adapter.MyCustomPagerAdapterObject;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.utils.AppConstans;
import com.kunj.mobistatus.utils.Utility;

import static android.content.Context.CLIPBOARD_SERVICE;

public class ProductSlidingFragment extends Fragment implements View.OnClickListener {

    //private static ProductSlidingFragment INSTANCE;
    public final static String FRAGMENT_TAG = "ProductSlidingFragment";
    private String headerName;
    ViewPager viewPager;
    private TextView pnumber;
    private static Long currentPage = 0L;
    private static ContentsApp lastPage;
    private static int NUM_PAGES = 8;//
    //private static int totalPage = 0;
    private MyCustomPagerAdapterObject adapter;
    private ArrayList<String> ImagesArray = new ArrayList<>();
    private Long categoryId = 0L;
    String TAG = "Adsysytem";


    private TextView imgSetSticker;
    private ImageView imgFavoriteIcon;
    private ImageView imgShareWhatsApp;
    private ImageView imgShatreAllApp;
    private AdView mAdView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bd = getArguments();
            headerName = bd.getString(AppConstans.HEADER_TITLE);
            currentPage = bd.getLong(AppConstans.CURRENT_CLICK_PRODUCT_ID, 0L);
            categoryId = bd.getLong(AppConstans.PASSING_DATA);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_sliding, container, false);
        ((MainActivity) getActivity()).setHeader(headerName);
        intiAdd(v);
        init(v);
        return v;
    }

    private void intiAdd(View v) {
        mAdView = v.findViewById(R.id.adView);
        MobileAds.initialize(v.getContext(), getResources().getString(R.string.app_id));
        AdRequest adRequest = new AdRequest.Builder().
                addTestDevice("539257B5547CB2D2B8774988D6418083").build();


        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "onAdLoaded: ");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "onAdFailedToLoad: ");
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                Log.d(TAG, "onAdOpened: ");
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                Log.d(TAG, "onAdLeftApplication: ");
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed: ");
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void init(View v) {
        imgSetSticker = v.findViewById(R.id.imgSetSticker);
        imgSetSticker.setOnClickListener(this);
        imgFavoriteIcon = v.findViewById(R.id.imgFavoriteIcon);
        imgFavoriteIcon.setOnClickListener(this);
        imgShareWhatsApp = v.findViewById(R.id.imgShareWhatsApp);
        imgShareWhatsApp.setOnClickListener(this);
        imgShatreAllApp = v.findViewById(R.id.imgShatreAllApp);
        imgShatreAllApp.setOnClickListener(this);
        (v.findViewById(R.id.imgClipData)).setOnClickListener(this);

        viewPager = v.findViewById(R.id.pager);
        pnumber = v.findViewById(R.id.pnumber);
        String quary = "Select * from " + NamingHelper.toSQLName(ContentsApp.class) + " where " + NamingHelper.toSQLNameDefault("category_id") + " = ? and " + NamingHelper.toSQLNameDefault("status ") + " = 1 order by id DESC";
        List<ContentsApp> contents = ContentsApp.findWithQuery(ContentsApp.class, quary, String.valueOf(categoryId));

        if (contents.size() > 0) {
            adapter = new MyCustomPagerAdapterObject(v.getContext(), contents);
            viewPager.setAdapter(adapter);
            int noOfPage = adapter.getItemPositionOnId(currentPage);
            viewPager.setCurrentItem(noOfPage);
            lastPage = adapter.getCurrentObjectData(noOfPage);
            lastPage.is_read = 1;
            lastPage.save();
            if (lastPage.is_favourite == 1) {
                imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
            }
            pnumber.setText((noOfPage + 1) + "/" + contents.size());


        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                ((MainActivity) getActivity()).categoryLastPosition = i;
                Log.d("categoryLastPosition", ((MainActivity) getActivity()).categoryLastPosition + "assign sliding fragment ");
                int total = viewPager.getAdapter().getCount();
                pnumber.setText(String.format("%d / %d", i + 1, total));
                lastPage = adapter.getCurrentObjectData(viewPager.getCurrentItem());
                if (lastPage.is_favourite == 1) {
                    imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
                } else {
                    imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red));
                }
                if (lastPage.is_read == 0) {
                    lastPage.is_read = 1;
                    lastPage.save();
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
    }

    /*public static ProductSlidingFragment getInstance(Bundle data) {

        if (INSTANCE == null) {
            INSTANCE = new ProductSlidingFragment();
        }
        INSTANCE.setArguments(data);
        return INSTANCE;
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgSetSticker:
                if (viewPager != null && adapter != null && adapter.getCurrentObjectData(viewPager.getCurrentItem()) != null) {
                    String data = adapter.getCurrentObjectData(viewPager.getCurrentItem()).text;
                    startActivity(new Intent(view.getContext(), AddingStickerActivity.class).putExtra("data", data));
                }
                break;
            case R.id.imgFavoriteIcon:
                if (lastPage != null) {
                    if (lastPage.is_favourite == 1) {
                        imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red));
                        lastPage.setIs_favourite(0);
                        lastPage.save();
                    } else {
                        imgFavoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_favourite_red_fill));
                        lastPage.setIs_favourite(1);
                        lastPage.save();
                    }
                }
                break;
            case R.id.imgShareWhatsApp:
                String textUrl = AppConstans.APPURL + "\n\n" + lastPage.text;
                Utility.shareWithApp(view.getContext(), textUrl, true);
                break;
            case R.id.imgShatreAllApp:
                String textUrl1 = AppConstans.APPURL + "\n\n" + lastPage.text;
                Utility.shareWithApp(view.getContext(), textUrl1, false);
                break;
            case R.id.imgClipData:
                ClipboardManager clipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(AppConstans.APPURL + "\n\n" + lastPage.text);
                Toast.makeText(view.getContext(), "Status clip successfully", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
