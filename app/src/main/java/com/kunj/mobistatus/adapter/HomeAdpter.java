package com.kunj.mobistatus.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.model.Homemain;
import com.kunj.mobistatus.utils.AppConstans;


public class HomeAdpter extends RecyclerView.Adapter<HomeAdpter.Holder> {
    ArrayList<Homemain> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    private int lastPosition = -1;

    public HomeAdpter(ArrayList<Homemain> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_home, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.title.setText(homeArrayList.get(position).getLanguage().getLanguage_name());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rec_sub.setLayoutManager(linearLayoutManager);
        holder.rec_sub.setItemAnimator(new DefaultItemAnimator());
        holder.rec_sub.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.HORIZONTAL));
        HomeSubAdpter apercuAdapter = new HomeSubAdpter(homeArrayList.get(position).getCategories());
        holder.rec_sub.setAdapter(apercuAdapter);

        holder.viewmore.setTag(R.id.headerTitle, homeArrayList.get(position).getLanguage().getLanguage_name());
        holder.viewmore.setTag(R.id.all, homeArrayList.get(position).getLanguage().getId());
        holder.viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bd = new Bundle();
                bd.putString(AppConstans.HEADER_TITLE, (String) v.getTag(R.id.headerTitle));
                bd.putString(AppConstans.PASSING_DATA, String.valueOf(v.getTag(R.id.all)));
                ((MainActivity) mContext).changeFragment(AppConstans.CATEGORY_FRAGMENT, bd);
            }
        });
        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView title, viewmore;
        RecyclerView rec_sub;

        public Holder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            rec_sub = itemView.findViewById(R.id.rec_sub);
            viewmore = itemView.findViewById(R.id.viewmore);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.bottom_to_top);
            animation.setDuration(1000);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
