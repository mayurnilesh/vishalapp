package com.kunj.mobistatus.adapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.utils.AppConstans;
import com.kunj.mobistatus.utils.Utility;

import static android.content.Context.CLIPBOARD_SERVICE;


public class ProductAdpter extends RecyclerView.Adapter<ProductAdpter.Holder> {
    ArrayList<ContentsApp> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    String headerTitle;

    public ProductAdpter(List<ContentsApp> homeArrayList, String headerTitle) {
        this.homeArrayList = (ArrayList<ContentsApp>) homeArrayList;
        this.headerTitle = headerTitle;
    }

    public void setData(List<ContentsApp> homeArrayList) {
        this.homeArrayList = (ArrayList<ContentsApp>) homeArrayList;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.product_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.imageFavourite.setTag(R.id.nav_view, homeArrayList.get(position));
        holder.detailText.setTag(R.id.nav_view, homeArrayList.get(position));
        if (homeArrayList.get(position).is_favourite == 1) {
            holder.imageFavourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favourite_red_fill));
        } else {
            holder.imageFavourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favourite_red));
        }
        if (homeArrayList.get(position).is_read == 0) {
            holder.txtNew.setVisibility(View.VISIBLE);
        } else {
            holder.txtNew.setVisibility(View.GONE);
        }
        holder.detailText.setText(homeArrayList.get(position).text);
        holder.detailText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentsApp id = (ContentsApp) v.getTag(R.id.nav_view);
                Bundle bd = new Bundle();
                bd.putLong(AppConstans.CURRENT_CLICK_PRODUCT_ID, id.getId());
                bd.putString(AppConstans.HEADER_TITLE, headerTitle);
                bd.putLong(AppConstans.PASSING_DATA, homeArrayList.get(position).getCategory_id());
                ((MainActivity) mContext).changeFragment(AppConstans.HORIZONTAL_SLIDING_TITLES, bd);

            }
        });

        holder.shareAllApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textUrl = AppConstans.APPURL + "\n\n" + homeArrayList.get(position).getText();
                Utility.shareWithApp(view.getContext(), textUrl, false);
            }
        });

        holder.shareWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textUrl = AppConstans.APPURL + "\n\n" + homeArrayList.get(position).getText();
                Utility.shareWithApp(view.getContext(), textUrl, true);
            }
        });

        holder.imgClipData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textUrl = AppConstans.APPURL + "\n\n" + homeArrayList.get(position).getText();
                ClipboardManager clipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(textUrl);
                Toast.makeText(view.getContext(), "Status clip successfully", Toast.LENGTH_LONG).show();
            }
        });

        holder.imageFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentsApp id = (ContentsApp) view.getTag(R.id.nav_view);
                if (id.is_favourite == 1) {
                    ((ImageView) view).setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favourite_red));
                    id.setIs_favourite(0);
                    id.save();
                } else {
                    ((ImageView) view).setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favourite_red_fill));
                    id.setIs_favourite(1);
                    id.save();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView detailText;
        TextView txtNew;
        CardView rl;
        ImageView shareAllApp;
        ImageView imageFavourite;
        ImageView shareWhatsApp, imgClipData;

        public Holder(View itemView) {
            super(itemView);

            detailText = itemView.findViewById(R.id.detailText);
            txtNew = itemView.findViewById(R.id.txtNew);
            shareAllApp = itemView.findViewById(R.id.shareAllApp);
            imgClipData = itemView.findViewById(R.id.imgClipData);
            imageFavourite = itemView.findViewById(R.id.imageFavourite);
            shareWhatsApp = itemView.findViewById(R.id.shareWhatsApp);
            rl = itemView.findViewById(R.id.rlmain);

        }
    }
}
