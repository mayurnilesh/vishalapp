package com.kunj.mobistatus.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.utils.AppConstans;


public class HomeSubAdpter extends RecyclerView.Adapter<HomeSubAdpter.Holder> {
    ArrayList<Category> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    private int lastPosition = -1;

    public HomeSubAdpter(ArrayList<Category> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_home_sub, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.title.setText(homeArrayList.get(position).getCategory_name());
        holder.rl.setBackgroundColor(Color.parseColor(homeArrayList.get(position).bg_color));

        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bd = new Bundle();
                bd.putString(AppConstans.HEADER_TITLE, homeArrayList.get(position).getCategory_name());
                bd.putLong(AppConstans.PASSING_DATA, homeArrayList.get(position).getId());
                ((MainActivity) mContext).changeFragment(AppConstans.PRODUCT_FRAGMNET, bd);
            }
        });

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView title;
        CardView rl;

        public Holder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            rl = itemView.findViewById(R.id.rlmain);

        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.right_to_in_child);
            animation.setDuration(1000);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
