package com.kunj.mobistatus.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.kunj.mobistatus.MainActivity;
import com.kunj.mobistatus.R;
import com.kunj.mobistatus.sugermodel.Category;
import com.kunj.mobistatus.sugermodel.ContentsApp;
import com.kunj.mobistatus.utils.AppConstans;
import com.orm.util.NamingHelper;


public class CategoryAdpter extends RecyclerView.Adapter<CategoryAdpter.Holder> {
    ArrayList<Category> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public CategoryAdpter(ArrayList<Category> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.title.setText(homeArrayList.get(position).getCategory_name());
        holder.rl.setBackgroundColor(Color.parseColor(homeArrayList.get(position).getBg_color()));
        long id = homeArrayList.get(position).getId();
        //long noOfData = ContentsApp.count(ContentsApp.class, " id = ?" , new String[]{String.valueOf(id)});

        holder.imgCatIcon.setImageDrawable(mContext.getResources().getDrawable(setIcon(homeArrayList.get(position).cat_type)));

        long noOfData = ContentsApp.count(ContentsApp.class, NamingHelper.toSQLNameDefault("category_id") + " = ? and " + NamingHelper.toSQLNameDefault("is_read") + " = ?", new String[]{String.valueOf(id), "0"});
        if (noOfData > 0) {
            holder.imgNoOfdata.setVisibility(View.VISIBLE);
            holder.imgNoOfdata.setText(String.valueOf(noOfData));
        } else {
            holder.imgNoOfdata.setVisibility(View.GONE);
        }
        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bd = new Bundle();
                bd.putString(AppConstans.HEADER_TITLE, homeArrayList.get(position).getCategory_name());
                bd.putLong(AppConstans.PASSING_DATA, homeArrayList.get(position).getId());

                ((MainActivity) mContext).changeFragment(AppConstans.PRODUCT_FRAGMNET, bd);

            }
        });

    }

    private int setIcon(String cat_type) {

        int id;
        if (cat_type.matches("blush")) {
            id = R.drawable.blush;
        } else if (cat_type.matches("courage")) {
            id = R.drawable.courage;
        } else if (cat_type.matches("crying")) {
            id = R.drawable.crying;
        } else if (cat_type.matches("date")) {
            id = R.drawable.date;
        } else if (cat_type.matches("idea")) {
            id = R.drawable.idea;
        } else if (cat_type.matches("joyful")) {
            id = R.drawable.joyful;
        } else if (cat_type.matches("shaking_hands")) {
            id = R.drawable.shaking_hands;
        } else if (cat_type.matches("wedding_cake")) {
            id = R.drawable.wedding_cake;
        } else if (cat_type.matches("lovelorn")) {
            id = R.drawable.lovelorn;
        } else {
            id = R.drawable.blush;
        }
        return id;

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView title;
        TextView imgNoOfdata;
        ImageView imgCatIcon;
        RelativeLayout rl;

        public Holder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            imgCatIcon = itemView.findViewById(R.id.imgCatIcon);
            imgNoOfdata = itemView.findViewById(R.id.imgNoOfdata);
            rl = itemView.findViewById(R.id.rlmain);

        }
    }
}
