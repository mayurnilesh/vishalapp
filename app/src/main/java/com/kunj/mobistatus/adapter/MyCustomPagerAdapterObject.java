package com.kunj.mobistatus.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.kunj.mobistatus.R;
import com.kunj.mobistatus.sugermodel.ContentsApp;


public class MyCustomPagerAdapterObject extends PagerAdapter {


    private ArrayList<ContentsApp> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public MyCustomPagerAdapterObject(Context context, List<ContentsApp> IMAGES) {
        this.context = context;
        this.IMAGES = (ArrayList<ContentsApp>) IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.product_slider, view, false);
        ((TextView) imageLayout.findViewById(R.id.title)).setText(IMAGES.get(position).text);
        assert imageLayout != null;

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    public ContentsApp getCurrentObjectData(int i) {
        return IMAGES.get(i);
    }

    public int getItemPositionOnId(Long id) {
        for (int i = 0; i < IMAGES.size(); i++) {
            if (IMAGES.get(i).getId().equals(id)) {
                return i;
            }
        }
        return 0;
    }

}