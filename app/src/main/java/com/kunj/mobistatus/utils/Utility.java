package com.kunj.mobistatus.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.kunj.mobistatus.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

    public static void hideSoftKeyPad(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void shareWithApp(Context c, String msg, Boolean isOnlyWhatsapp) {
        if (isOnlyWhatsapp) {
            try {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,  msg);
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.whatsapp");
                c.startActivity(sendIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(c, "WhatsApp not Found", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(c, "Sharing with Whatsapp make issue.", Toast.LENGTH_LONG).show();
            }
        } else {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, c.getResources().getString(R.string.dummy)+msg);
            c.startActivity(Intent.createChooser(sharingIntent, "Today quote"));
        }
    }

    private static String getDateToString(Date date) {
        String dateTime = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateTime = dateFormat.format(date);
            System.out.println("Current Date Time : " + dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateTime;
    }


}
