package com.kunj.mobistatus.utils;

public class AppConstans {

    public static final String HEADER_TITLE = "header";
    public static final String CURRENT_CLICK_PRODUCT_ID = "current_click_id";
    //  private static final String BASEURL = "http://kunjinfotech.com/MyStatus/web/api/v1/";
    private static final String BASEURL = "http://kunjinfotech.com/Mobistatus/web/api/v1/mobistatus_main.php";
    public static final String APPURL = "https://play.google.com/store/apps/details?id=com.kunj.mobistatus";

    /*Fragment Work */
    public static final int MAIN_FRAGMENT = 221;
    public static final int CATEGORY_FRAGMENT = 222;
    public static final int PRODUCT_FRAGMNET = 223;
    public static final int HORIZONTAL_SLIDING_TITLES = 224;
    public static final int FAVOURITE_LIST = 225;
    public static final int DISCLAIMER_FRAGMENT = 226;
    public static final int RATE_US_FRAGMENT = 227;

    /*Application Session*/
    public static final String IS_USER_REGISTER = "is_user_register";
    public static final String IS_NOTIFICATION_ENABLE = "is_notification_enable";
    public static final String IS_SECOND_TIME = "is_second_time";
    public static final String BACKGROUND_URL = "background_url";
    /*Urls*/
    // public static final String API_REGISTRATION = BASEURL + "index.php";
    public static final String API_REGISTRATION = BASEURL;

    public class Registrarion {
        public static final String NOTIFICATION = "UNOTIFICATION";
        public static final String REGISTER = "UREG";
        public static final String DEVICE_TYPE = "android";
        public static final String CATSYNC = "CATSYNC";
        public static final String UCHECK = "UCHECK";
        public static final String LANGSYNC = "LANGSYNC";
        public static final String CANTSYNC = "CANTSYNC";
        public static final String URATE = "URATING";
        public static final String UFEEDBACK = "UFEEDBACK";

    }

    /*Passis */
    public static final int PASSING_ID = 505;
    public static final String PASSING_DATA = "id";


    /*App sync*/
    public static final String IS_LUNGAUGE_UPDATE = "is_language_update";
    public static final String IS_CATEGORY_UPDATE = "is_category_update";
    public static final String IS_CONTENT_UPDATE = "is_content_update";
    public static final String IS_NEW_UPDATE = "is_new_update";
    public static final String IS_RATING_DONE = "is_rating_done";
    public static final String IS_SHOW_RATING = "is_show_rating";


    public static final String FCM_TOKEN = "fcm_token";

}
