package com.kunj.mobistatus.utils;

import android.content.Context;
import android.os.Build;

import com.orm.util.NamingHelper;

import java.util.ArrayList;
import java.util.List;

import com.kunj.mobistatus.R;
import com.kunj.mobistatus.model.DrawerMainModel;
import com.kunj.mobistatus.model.DrawerSubItem;
import com.kunj.mobistatus.sugermodel.Language;

public class DrawerConstants {

    /*Main Menu*/
    public final static String nav_home = "Home";
    public final static String nav_favourite = "My Favourite";
    public final static String nav_languages = "Language";
    public final static String nav_disclaimer = "Disclaimer";
    public final static String nav_rate_us = "Feedback";
    public final static String nav_share = "Share app";


    public static ArrayList<DrawerMainModel> getDrawerData(Context context) {
        ArrayList<DrawerMainModel> items = new ArrayList<>();
        items.add(createMenuClass(context, R.drawable.ic_home_black, DrawerConstants.nav_home, new ArrayList<DrawerSubItem>()));
        items.add(createMenuClass(context, R.drawable.ic_favourite_fill_black, DrawerConstants.nav_favourite, new ArrayList<DrawerSubItem>()));

        ArrayList<DrawerSubItem> drawerActivity = new ArrayList<>();

        //String addtionCondition= " and exist (select 1 from "+NamingHelper.toSQLName(Category.class)+" where "+NamingHelper.toSQLName(Language.class)+".id = "+NamingHelper.toSQLName(Category.class)+".id)";
        String query = "select * from " + NamingHelper.toSQLName(Language.class) + " where " + NamingHelper.toSQLNameDefault("status ") + " = 1";
        //String query = "select * from " + NamingHelper.toSQLName(Language.class) + " where " + NamingHelper.toSQLNameDefault("status ") + " = 1"+addtionCondition;
        List<Language> languages = Language.findWithQuery(Language.class, query);
        if (languages.size() > 0) {
            for (int i = 0; i < languages.size(); i++) {
                drawerActivity.add(new DrawerSubItem(languages.get(i).getLanguage_name(), DrawerConstants.nav_languages, String.valueOf(languages.get(i).getId())));
            }

            items.add(createMenuClass(context, R.drawable.ic_favourite_book, DrawerConstants.nav_languages, drawerActivity));
        } else {
            items.add(createMenuClass(context, R.drawable.ic_favourite_book, DrawerConstants.nav_languages, new ArrayList<DrawerSubItem>()));
        }
        items.add(createMenuClass(context, R.drawable.ic_disclimer, DrawerConstants.nav_disclaimer, new ArrayList<DrawerSubItem>()));
       // items.add(createMenuClass(context, R.drawable.ic_rate_us, DrawerConstants.nav_rate_us, new ArrayList<DrawerSubItem>()));
        items.add(createMenuClass(context, R.drawable.ic_share_black, DrawerConstants.nav_share, new ArrayList<DrawerSubItem>()));


        return items;
    }

    private static DrawerMainModel createMenuClass(Context context, int id, String name, ArrayList<DrawerSubItem> subItems) {
        DrawerMainModel drawerMainModel = new DrawerMainModel();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawerMainModel.resurceId = context.getDrawable(id);
        } else {
            drawerMainModel.resurceId = context.getResources().getDrawable(id);
        }
        drawerMainModel.name = name;
        drawerMainModel.subitems = subItems;
        drawerMainModel.isDropDown = subItems.size() > 0;
        return drawerMainModel;
    }

}
