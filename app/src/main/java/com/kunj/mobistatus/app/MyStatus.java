package com.kunj.mobistatus.app;

import android.app.Application;
import android.content.Context;

import com.orm.SugarContext;

/**
 * Created by Administrator on 12-Mar-18.
 */

public class MyStatus extends Application {

    private static MyStatus mInstance;

    public MyStatus() {
        mInstance = this;
    }

    public static Context getContext() {
        return mInstance;
    }

    public static final String TAG = MyStatus.class.getSimpleName();

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SugarContext.init(this);

//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .cacheOnDisc(true)
//                .cacheInMemory(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .resetViewBeforeLoading(true)
//                .showImageOnFail(R.drawable.ic_launcher_background)
//                .showImageForEmptyUri(R.drawable.ic_launcher_background)
//                .displayer(new FadeInBitmapDisplayer(300)).build();
//
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .discCacheSize(100 * 1024 * 1024).build();
//
//        ImageLoader.getInstance().init(config);
        ApplicationSession.setApplicationContext(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
