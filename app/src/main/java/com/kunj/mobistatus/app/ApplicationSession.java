package com.kunj.mobistatus.app;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class ApplicationSession {
    private static String TAG = "ApplicationSession";
    static SharedPreferences preferences = null;

    public static void setApplicationContext(Context context) {
        try {
            preferences = context.getSharedPreferences("upreference", 0);
        } catch (NullPointerException e) {
            Log.e(TAG, "setApplicationContext " + e.toString());
        }
    }

    public static void setToPreference(String fieldName, String fieldValue) {
        try {
            Editor editor = preferences.edit();
            editor.putString(fieldName, fieldValue);
            editor.putString(fieldName, fieldValue);
            editor.commit();
        } catch (NullPointerException e) {
            Log.e(TAG, "setToPreference " + e.toString());
        }
    }

    public static void setBoleanToPreference(String fieldName, boolean fieldValue) {
        try {
            Editor editor = preferences.edit();
            editor.putBoolean(fieldName, fieldValue);
            editor.commit();
        } catch (Exception e) {
            Log.e(TAG, "setToBoleanPreference " + e.toString());
        }
    }

    public static boolean getBooleanFromPreference(String fieldName) {
        boolean fiedValue = false;
        try {
            fiedValue = preferences.getBoolean(fieldName, false);
        } catch (Exception e) {
            Log.e(TAG, "getBooleanFromPreference " + e.toString());
        }
        return fiedValue;
    }

    public static String getFromPreference(String fieldName) {
        String fieldValue = null;
        try {
            fieldValue = preferences.getString(fieldName, "");
        } catch (NullPointerException e) {
            Log.e(TAG, "getFromPreference " + e.toString());
        }
        return fieldValue;
    }

    public static void clearPreference(String fieldName) {
        try {
            Editor editor = preferences.edit();
            editor.remove(fieldName);
            editor.commit();
        } catch (NullPointerException e) {
            Log.e(TAG, "clearPreference " + e.toString());
        }
    }

    public static void clearAllPreference() {
        String guestToken = "";

        Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        editor.commit();

    }
}
